fs = require 'fs'
should = require 'should'
memoBuilder = require '../lib/memo'
{keys, mkTemp, rmDir} = require './test-helper'

describe 'lib/memo', ->

  memoAttributes = ['getStorageDir', 'readFile', 'readJSON', 'writeFile', 'writeJSON']

  describe 'initialize', ->

    temp = null
    beforeEach -> temp = do mkTemp
    afterEach -> rmDir temp

    it 'simple create', ->
      memo = memoBuilder temp
      keys(memo).should.be.deepEqual memoAttributes
      memo.getStorageDir().should.be.equal temp

    it 'crash if receive a file as parameter', ->
      file = temp + '/myfile'
      fs.writeFileSync file, 'Some text.'
      (-> memoBuilder file).should.throw 'memoBuilder must receive a directory'

    it 'create config dir', ->
      newDir = temp + '/nonexistent'
      memo = memoBuilder newDir
      keys(memo).should.be.deepEqual memoAttributes
      memo.getStorageDir().should.be.equal newDir

  describe 'write files', ->

    tempStorageDir = null
    memo = null
    beforeEach ->
      tempStorageDir = do mkTemp
      memo = memoBuilder tempStorageDir
    afterEach ->
      rmDir tempStorageDir

    it 'as a generic file', (done)->
      memo.writeFile 'myfile', 'some text', (err)->
        data = fs.readFileSync "#{tempStorageDir}/myfile", 'utf8'
        data.should.be.equal 'some text'
        done err

    it 'as JSON', (done)->
      memo.writeJSON 'myfile', {a:111, b:222}, (err)->
        data = fs.readFileSync "#{tempStorageDir}/myfile.json", 'utf8'
        data.replace(/\s/g,'').should.be.equal '{"a":111,"b":222}'
        done err

  describe 'read files', ->

    tempStorageDir = null
    memo = null
    before ->
      tempStorageDir = do mkTemp
      memo = memoBuilder tempStorageDir
      fs.writeFileSync "#{tempStorageDir}/myfile.txt", 'Hello'
      fs.writeFileSync "#{tempStorageDir}/myfile.json", '{"a":11,"b":22}'
    after ->
      rmDir tempStorageDir

    it 'as a generic file', ->
      data = memo.readFile 'myfile.txt'
      data.should.be.equal 'Hello'

    it 'with null when inexistent', ->
      data = memo.readFile 'nonexistent'
      should(data).be.null()

    it 'with default value when inexistent and default value provided', ->
      data = memo.readFile 'nonexistent', 'Hi'
      data.should.be.equal 'Hi'

    it 'as JSON', ->
      data = memo.readJSON 'myfile'
      data.should.be.deepEqual {a:11, b:22}

    it 'with empty object when inexistent JSON', ->
      data = memo.readJSON 'inexistent'
      data.should.be.deepEqual {}

