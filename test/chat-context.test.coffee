fs = require 'fs'
should = require 'should'
{ silenceOutputs, restoreOutputs,
  keys, mkTemp, rmDir, timeout } = require './test-helper'
ChatContext = require '../lib/chat-context'
Bot = require '../telegram-bot'

describe 'lib/chat-context', ->

  @timeout 2000

  before silenceOutputs
  after restoreOutputs

  temp = null
  bot = null
  beforeEach ->
    temp = do mkTemp
    bot = new Bot \
      name:'My Bot', username:'mybot',
      token:'ABCD', admChatID:'1234',
      configDir: temp
  afterEach -> rmDir temp

  it 'read context data', ->
    fs.writeFileSync "#{temp}/context-123.json", '{"a":11,"b":22}'
    chatCtx = new ChatContext bot, 123
    chatCtx.get('a').should.be.equal 11
    chatCtx.get('b').should.be.equal 22

  it 'set and write context data, from object', (done)->
    chatCtx = new ChatContext bot, 123
    chatCtx.set c: 33
    chatCtx.set d: 44, e: 55, f:[66,77,88]
    chatCtx.get('c').should.be.equal 33
    chatCtx.get('d').should.be.equal 44
    chatCtx.get('f').should.be.deepEqual [66,77,88]
    timeout 1.8, ->
      json = JSON.parse fs.readFileSync "#{temp}/context-123.json", 'utf8'
      json.should.be.deepEqual lang:'en', c:33, d:44, e:55, f:[66,77,88]
      do done

  it 'set and write context data, from arg list', ->
    chatCtx = new ChatContext bot, 123
    chatCtx.set 'c', 33
    chatCtx.set 'd', 44, 'e', 55, 'f', [66,77,88]
    chatCtx.get('c').should.be.equal 33
    chatCtx.get('d').should.be.equal 44
    chatCtx.get('f').should.be.deepEqual [66,77,88]

  describe 'send kinds of message', ->

    it 'sendMsg'

    it 'sendMD'

    it 'sendHTML'

