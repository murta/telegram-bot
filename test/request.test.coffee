should = require 'should'
EventEmitter = require 'events'

{protocolMod, request, computeRedirection} = require '../lib/request'
httpMod = require 'http'

{keys, A, timeout, silenceOutputs, restoreOutputs} = require './test-helper'

describe 'lib/request', ->
  @timeout 6e3

  before silenceOutputs
  after restoreOutputs

  describe 'computeRedirection', ->

    it 'to a new query', ->
      computeRedirection('http://aa.bb/cc', '?x=1')
        .should.be.equal 'http://aa.bb/cc?x=1'
      computeRedirection('http://aa.bb/cc?x=1', '?y=2')
        .should.be.equal 'http://aa.bb/cc?y=2'

    it 'to a full URL', ->
      computeRedirection('http://aa.bb/cc', 'http://dd.ee/ff')
        .should.be.equal 'http://dd.ee/ff'

    it 'to a full URL without protocol', ->
      computeRedirection('http://aa.bb/cc', '//dd.ee/ff')
        .should.be.equal 'http://dd.ee/ff'
      computeRedirection('https://aa.bb/cc', '//dd.ee/ff')
        .should.be.equal 'https://dd.ee/ff'

    it 'to a new path from root', ->
      computeRedirection('http://aa.bb/', '/new/path')
        .should.be.equal 'http://aa.bb/new/path'
      computeRedirection('http://aa.bb/?x=1', '/new/path')
        .should.be.equal 'http://aa.bb/new/path'
      computeRedirection('http://aa.bb/cc', '/new/path')
        .should.be.equal 'http://aa.bb/new/path'
      computeRedirection('http://aa.bb/', '/new/path?x=1')
        .should.be.equal 'http://aa.bb/new/path?x=1'

    it 'to a relative path', ->
      computeRedirection('http://aa.bb/cc', 'file.ext')
        .should.be.equal 'http://aa.bb/file.ext'
      computeRedirection('http://aa.bb/cc/', 'file.ext')
        .should.be.equal 'http://aa.bb/cc/file.ext'
      computeRedirection('http://aa.bb/cc/dd', 'file.ext')
        .should.be.equal 'http://aa.bb/cc/file.ext'
      computeRedirection('http://aa.bb/cc/dd', 'file.ext?x=1')
        .should.be.equal 'http://aa.bb/cc/file.ext?x=1'

  describe 'request', ->

    mkBuffer = (hexStr)->
      new Buffer ( parseInt byte, 16 for byte in hexStr.split ' ' )

    class FakeHttpChunks extends EventEmitter
      constructor: (@url, @callback)->
        @res = new EventEmitter
        @res.headers = {}
        if @url.host is 'ok.domain'
          @res.statusCode = 200
        if @url.host is 'gzip.domain'
          @res.statusCode = 200
          @res.headers = { 'Content-Encoding': 'gzip' }
        if @url.host is 'notfound.domain'
          @res.statusCode = 404
          @res.statusMessage = 'Not Found'
        if @url.host is 'srverror.domain'
          @res.statusCode = 500
          @res.statusMessage = 'Internal Server Error'
        if @url.host is 'moved.domain'
          @res.statusCode = 301
          @res.headers = { location: 'http://ok.domain' }
      end: ->
        @callback @res
        if @url.host is 'ok.domain'
          timeout .02, => @res.emit 'data', 'ABC['
          timeout .03, => @res.emit 'data', @url.host
          timeout .04, => @res.emit 'data', ']DEF'
        if @url.host is 'gzip.domain'
          buf1 = mkBuffer '1f 8b 08 08 33 8d 53 57 00 03 62 6c 69'
          buf2 = mkBuffer '2e 74 78 74 00 0b ce cf 4d 55 28 49 ad'
          buf3 = mkBuffer '28 d1 03 00 bd 01 72 6a 0a 00 00 00'
          timeout .02, => @res.emit 'data', buf1
          timeout .03, => @res.emit 'data', buf2
          timeout .04, => @res.emit 'data', buf3
        timeout .05, => @res.emit 'end'

    fakeHttp = ->
      request: (url, cb)-> new FakeHttpChunks url, cb
      Agent: httpMod.Agent

    after -> protocolMod.http = httpMod

    it 'get example.com by http', (done)->
      request('http://example.com')
        .then (res)->
          res.statusCode.should.be.equal 200
          res.data.should.be.match /Example Domain/
          do done
        .catch done

    it 'get example.com by https', (done)->
      request('https://example.com')
        .then (res)->
          res.statusCode.should.be.equal 200
          res.data.should.be.match /Example Domain/
          do done
        .catch done

    it 'get example.com 404', (done)->
      request('http://example.com/do/not/exists')
        .then (res)-> done Error 'it must fail'
        .catch (err)->
          err.statusCode.should.be.equal 404
          err.message.should.be.equal 'Not Found'
          do done
        .catch done

    it 'get some page data', (done)->
      protocolMod.http = do fakeHttp
      request('http://ok.domain')
        .then (res)->
          res.statusCode.should.be.equal 200
          res.data.should.be.equal 'ABC[ok.domain]DEF'
          do done
        .catch done

    it 'get 404', (done)->
      protocolMod.http = do fakeHttp
      request('http://notfound.domain')
        .then (res)-> done Error 'it must fail'
        .catch (err)->
          err.statusCode.should.be.equal 404
          err.message.should.be.equal 'Not Found'
          do done
        .catch done

    it 'get 500', (done)->
      protocolMod.http = do fakeHttp
      request('http://srverror.domain')
        .then (res)-> done Error 'it must fail'
        .catch (err)->
          err.statusCode.should.be.equal 500
          err.message.should.be.equal 'Internal Server Error'
          do done
        .catch done

    it 'get some page data as gzip', (done)->
      protocolMod.http = do fakeHttp
      request('http://gzip.domain')
        .then (res)->
          res.statusCode.should.be.equal 200
          res.data.should.be.equal 'Some text.'
          do done
        .catch done

    it 'get some page data after redrect', (done)->
      protocolMod.http = do fakeHttp
      request('http://moved.domain')
        .then (res)->
          res.statusCode.should.be.equal 200
          res.data.should.be.equal 'ABC[ok.domain]DEF'
          do done
        .catch done

