should = require 'should'

ddg = require '../lib/ddg'

{keys, A, silenceOutputs, restoreOutputs} = require './test-helper'

describe 'lib/ddg', ->
  @timeout 6e3

  before silenceOutputs
  after restoreOutputs

  it 'makes Accept-Language http header', ->
    ddg.mkAcceptLang('pt-BR', 'pt', 'en').should.be.equal 'pt-BR;q=1.00,pt;q=0.90,en;q=0.80'
    ddg.mkAcceptLang('A', 'B', 'C', 'D', 'E').should.be.equal 'A;q=1.00,B;q=0.90,C;q=0.80,D;q=0.70,E;q=0.60'

  it 'finds a query with open API', (done)->
    ddg 'Raul Seixas', ['pt-BR', 'pt', 'en'], (err, resp)->
      should(err).be.null()
      resp.url.should.match /^https?:\/\/.*Raul.*Seixas/i
      resp.content.should.match /Raul.*Seixas/i
      do done

  it 'makes query lang for site API', ->
    ddg.mkQueryLang('pt-BR').should.be.equal 'br-pt'
    ddg.mkQueryLang('pt').should.be.equal 'pt'

  it 'finds a query with site API', (done)->
    ddg.siteAPI 'huebr', ['pt-BR', 'pt', 'en'], (err, resp)->
      should(err).be.null()
      resp.url.should.match /^https?:\/\/.*/i
      resp.content.should.match /huebr/i
      resp.title.should.match /huebr/i
      do done

  it 'notify when cant find the the query', (done)->
    ddg 'InexistentXword InexistentYcontext InexistentW', ['pt'], (err, resp)->
      err.message.should.be.equal 'no result.'
      should(resp).be.not.ok()
      do done
