should = require 'should'
{askForGameRE, TicTacToe, listen} = require '../../interaction/tictactoe'
{ silenceOutputs, restoreOutputs,
  lastTelegramApiCall, rmLastTelegramApiCall,
  mkChatCtx, mkMsgCtx} = require '../test-helper'

describe 'interaction/tictactoe', ->

  before silenceOutputs
  after restoreOutputs
  afterEach rmLastTelegramApiCall

  it 'match ask for game message', ->
    'jogo da velha'.should.match askForGameRE
    'Jogo da velha!'.should.match askForGameRE
    'Vamos jogar velha?'.should.match askForGameRE

  describe 'the game object', ->

    msgCtx = null
    beforeEach ->
      msgCtx = mkMsgCtx text: 'velha'

    it 'know the game is started', ->
      game = new TicTacToe msgCtx
      game.usrIsPlaying().should.be.equal false
      game.init()
      game.usrIsPlaying().should.be.equal true

    it 'know the game is started, from memory', ->
      msgCtx.chatCtx.set 'tictactoe', "#{msgCtx.from.id}": { set: [[0,0,0],[0,0,0],[0,0,0]] }
      game = new TicTacToe msgCtx
      game.usrIsPlaying().should.be.equal true

    it 'mark the user choice', ->
      game = new TicTacToe msgCtx
      game.robotPlay = (->)
      game.init()
      game.set.should.be.deepEqual [[0,0,0],[0,0,0],[0,0,0]]
      game.humanPlay 'b2'
      game.set.should.be.deepEqual [[0,0,0],[0,1,0],[0,0,0]]

    it 'mark the user and bot choices', ->
      game = new TicTacToe msgCtx
      game.init()
      game.set.should.be.deepEqual [[0,0,0],[0,0,0],[0,0,0]]
      game.humanPlay 'b2'
      set = game.set[0]; set.push game.set[1]...; set.push game.set[2]...
      set.join().should.match /^([^1]*1[^1]*){2}$/
      set.reduce((s,i)-> s+i).should.be.equal 0
      game.set = [[0,0,0],[0,0,0],[1,-1,0]]
      game.humanPlay 'b2'
      set = game.set[0]; set.push game.set[1]...; set.push game.set[2]...
      set.join().should.match /^([^1]*1[^1]*){4}$/
      set.reduce((s,i)-> s+i).should.be.equal 0

    it 'recognize the winner', ->
      game = new TicTacToe msgCtx
      game.init()
      game.set = [[0,0,0],[0,1,0],[0,0,0]]
      game.testWinner().should.be.equal false
      game.set = [[0,1,0],[0,1,0],[0,1,-1]]
      game.testWinner().should.be.equal 'HUMAN'
      game.set = [[0,0,0],[1,1,1],[0,0,-1]]
      game.testWinner().should.be.equal 'HUMAN'
      game.set = [[1,0,0],[0,1,0],[0,-1,1]]
      game.testWinner().should.be.equal 'HUMAN'
      game.set = [[0,-1,0],[0,-1,0],[0,-1,1]]
      game.testWinner().should.be.equal 'BOT'
      game.set = [[0,0,0],[-1,-1,-1],[0,-0,1]]
      game.testWinner().should.be.equal 'BOT'
      game.set = [[-1,0,0],[0,-1,0],[0,1,-1]]
      game.testWinner().should.be.equal 'BOT'
      game.set = [[-1,1,-1],[1,-1,1],[1,-1,1]]
      game.testWinner().should.be.equal 'TIE'

