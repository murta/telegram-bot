should = require 'should'
{helloRE, listen} = require '../../interaction/hi'
{ silenceOutputs, restoreOutputs, mkMsgCtx,
  lastTelegramApiCall, rmLastTelegramApiCall } = require '../test-helper'

describe 'interaction/hi', ->

  before silenceOutputs
  after restoreOutputs
  afterEach rmLastTelegramApiCall

  it 'match hello expressions in a message', ->
    'hi'.should.match helloRE
    'hi!'.should.match helloRE
    ' hi ! '.should.match helloRE
    'Ooooiii?'.should.match helloRE
    'hi hehehe!'.should.not.match helloRE

  it 'answer to a hello message', ->
    msgCtx = mkMsgCtx text: 'hi!'
    do rmLastTelegramApiCall
    listen msgCtx
    lastTelegramApiCall().cmd.should.be.equal 'sendMessage'
    lastTelegramApiCall().opts.chat_id.should.be.equal 123
    lastTelegramApiCall().opts.text.should.match /^(Oi|Olá).*$/

