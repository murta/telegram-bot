should = require 'should'
{speakRE, speakREtoMe, listen, translate} = require '../../interaction/speak'
{ silenceOutputs, restoreOutputs, lastLog, rmLastLog, mkMsgCtx,
  lastTelegramApiCall, rmLastTelegramApiCall } = require '../test-helper'

describe 'interaction/speak', ->

  before silenceOutputs
  after restoreOutputs
  afterEach ->
    do rmLastTelegramApiCall
    do rmLastLog

  it 'match "say" expressions in a message', ->
    ('diga oi!'.match speakRE)[3].should.be.equal 'oi!'
    ('fale oi!'.match speakRE)[3].should.be.equal 'oi!'
    should('agora fale oi!'.match speakRE).be.null()
    ('agora fale oi!'.match speakREtoMe)[3].should.be.equal 'oi!'
    ('diga 1 2 3 testando.'.match speakRE)[3].should.be.equal '1 2 3 testando.'
    ('diga: oi!'.match speakRE)[3].should.be.equal 'oi!'
    ('diga que a mosca voa.'.match speakRE)[3].should.be.equal 'a mosca voa.'

  it 'answer with a voice message', ->
    listen mkMsgCtx text: 'diga oi'
    lastTelegramApiCall().cmd.should.be.equal 'sendVoice'
    lastTelegramApiCall().opts.file.length.should.be.greaterThan 1000
    lastTelegramApiCall().opts.chat_id.should.be.equal 123

  it 'only answer with voice a non direct message, if it starts with the command', ->
    msgCtx = mkMsgCtx text: 'hey diga oi'
    rmLastTelegramApiCall()
    listen msgCtx
    lastTelegramApiCall().should.be.deepEqual {}

  it 'answer with voice a direct message, even if the command is in the middle', ->
    listen mkMsgCtx text: 'bot, hey diga oi'
    lastTelegramApiCall().cmd.should.be.equal 'sendVoice'
    lastTelegramApiCall().opts.file.length.should.be.greaterThan 1000
    lastTelegramApiCall().opts.chat_id.should.be.equal 123
    rmLastTelegramApiCall()
    listen mkMsgCtx text: 'hey diga oi bot'
    lastTelegramApiCall().cmd.should.be.equal 'sendVoice'
    lastTelegramApiCall().opts.file.length.should.be.greaterThan 1000
    lastTelegramApiCall().opts.chat_id.should.be.equal 123

  it 'translate to speak', ->
    listen mkMsgCtx text: 'diga você é um robô!'
    lastLog().text.should.be.equal 'eu sou um robô!'
    listen mkMsgCtx text: 'fale você é idiota.'
    lastLog().text.should.be.equal 'eu sou pouco inteligente.'

