should = require 'should'
{pronoms:trPron, badWords:trBW} = require '../lib/translate'

describe 'lib/translate', ->

  it 'translate pronoms', ->
    trPron('você é um robô!').should.be.equal 'eu sou um robô!'
    trPron('vc vai reiniciar.').should.be.equal 'eu vou reiniciar.'
    trPron('eu tenho olhos.').should.be.equal 'você tem olhos.'
    trPron('ontem eu fui viajar.').should.be.equal 'ontem você foi viajar.'
    trPron('vc vai reiniciar.').should.be.equal 'eu vou reiniciar.'
    trPron('Ana mandou vc reiniciar.').should.be.equal 'Ana mandou eu reiniciar.'
    trPron('vc vai cair e você vai quebrar.').should.be.equal 'eu vou cair e eu vou quebrar.'
    trPron('eu vou subir e você vai descer.').should.be.equal 'você vai subir e eu vou descer.'
    trPron('isso é meu.').should.be.equal 'isso é seu.'
    trPron('isso é seu.').should.be.equal 'isso é meu.'
    trPron('isso é teu.').should.be.equal 'isso é meu.'
    trPron('meu dinheiro.').should.be.equal 'seu dinheiro.'
    trPron('seu dinheiro.').should.be.equal 'meu dinheiro.'
    trPron('teu dinheiro.').should.be.equal 'meu dinheiro.'
    trPron('você é meu robô.').should.be.equal 'eu sou seu robô.'
    trPron('eu sou seu tutor.').should.be.equal 'você é meu tutor.'

  it 'translate bad words', ->
    trBW('seu cú!').should.be.equal 'seu ânus!'
    trBW('você é idiota.').should.be.equal 'você é pouco inteligente.'
    trBW('você é um idiota.').should.be.equal 'você é pouco inteligente.'
    trBW('vocês são idiotas.').should.be.equal 'vocês são pouco inteligentes.'
    trBW('vocês são uns idiotas.').should.be.equal 'vocês são pouco inteligentes.'
    trBW('você é um cuzão idiota.').should.be.equal 'você é um ânus pouco inteligente.'
    trBW('vai se fudê!').should.be.equal 'vai fornicar consigo mesmo!'
    trBW('isso é uma bosta.').should.be.equal 'isso é um cocôzinho.'
    trBW('seus merdas.').should.be.equal 'seus cocôzinhos.'

  it 'translate mix', ->
    trPron(trBW 'seu cú!').should.be.equal 'meu ânus!'
    trPron(trBW 'você é idiota.').should.be.equal 'eu sou pouco inteligente.'
    trPron(trBW 'você é um idiota.').should.be.equal 'eu sou pouco inteligente.'

