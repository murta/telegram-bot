should = require 'should'
MessageContext = require '../lib/message-context'
{ silenceOutputs, restoreOutputs, mkChatCtx } = require './test-helper'

describe 'lib/message-context', ->

  chatCtx = null
  before ->
    do silenceOutputs
    chatCtx = mkChatCtx 123
  after restoreOutputs

  describe 'MessageContext instance has...', ->

    msgCtx = null
    beforeEach ->
      msgCtx = new MessageContext {text:'nothing', from:username:'nobody'}, chatCtx

    it 'ChatContext instance reference', ->
      should(msgCtx.chatCtx.bot.username).be.equal 'mybot'

    it 'original message object', ->
      msgCtx.srcMessage.should.be.deepEqual text:'nothing', from:username:'nobody'

    it 'message´s `from` object', ->
      msgCtx.from.should.be.deepEqual username:'nobody'

  describe 'processed message text', ->

    it 'recognize bot reference and remove it from text', ->
      msgCtx = new MessageContext {text:'Hi'}, chatCtx
      msgCtx.toMe.should.be.not.ok
      msgCtx.text.should.be.equal 'Hi'
      msgCtx = new MessageContext {text:'Hi bot'}, chatCtx
      msgCtx.toMe.should.be.ok
      msgCtx.text.should.be.equal 'Hi'
      msgCtx = new MessageContext {text:'@mybot Hi'}, chatCtx
      msgCtx.toMe.should.be.ok
      msgCtx.text.should.be.equal 'Hi'

  describe 'send kinds of answer message', ->

    it 'answer'

    it 'answerMD'

    it 'answerHTML'


