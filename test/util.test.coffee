should = require 'should'

util = require '../lib/util'

{keys} = require './test-helper'

describe 'lib/util', ->

  describe 'logger', ->

    lastLog = null
    lastLogObj = null
    before ->
      console.origLog = console.log
      console.log = (str)-> lastLog = str
      util.origLogWriter = util.logWriter
      util.logWriter = (obj)->
        lastLogObj = obj
        util.origLogWriter obj
    after ->
      console.log = console.origLog
      util.logWriter = util.origLogWriter

    it 'prints log in one line', ->
      util.origLogWriter a:111, b:222, c:'one\nthwo'
      lastLog.should.be.equal '{"a":111,"b":222,"c":"one\\nthwo"}'

    it 'log.trace', ->
      util.log.trace 'test'
      lastLog.should.match /\{"\$date":"[^"]+","\$type":"trace","\$msg":"test"\}/

    it 'log.debug', ->
      util.log.debug 'test'
      lastLog.should.match /\{"\$date":"[^"]+","\$type":"debug","\$msg":"test"\}/

    it 'log.info', ->
      util.log 'test'
      lastLog.should.match /\{"\$date":"[^"]+","\$type":"info","\$msg":"test"\}/
      util.log.info 'test'
      lastLog.should.match /\{"\$date":"[^"]+","\$type":"info","\$msg":"test"\}/

    it 'log.warn', ->
      util.log.warn 'test'
      lastLog.should.match /\{"\$date":"[^"]+","\$type":"warn","\$msg":"test"\}/

    it 'log.error', ->
      util.log.error 'test'
      lastLog.should.match /\{"\$date":"[^"]+","\$type":"error","\$msg":"test"\}/

    it 'log.critical', ->
      util.log.critical 'test'
      lastLog.should.match /\{"\$date":"[^"]+","\$type":"critical","\$msg":"test"\}/

    it 'log with date', ->
      util.log 'test'
      date = lastLog.replace /\{"\$date":"([^"]+)".*/, '$1'
      date.should.match /[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]+Z/
      lastLogObj.$date.constructor.should.be.equal Date
      date.should.be.equal lastLogObj.$date.toJSON()

    it 'log with only one string as args', ->
      util.log 'test', 'ABC'
      keys(lastLogObj).should.be.deepEqual ['$date','$msg','$type','0']
      lastLogObj.$msg.should.be.equal 'test'
      lastLogObj[0].should.be.equal 'ABC'

    it 'log with only one number as args', ->
      util.log 'test', 123
      keys(lastLogObj).should.be.deepEqual ['$date','$msg','$type','0']
      lastLogObj[0].should.be.equal 123

    it 'log with many arguments as array', ->
      util.log 'test', 111, 222, 333
      keys(lastLogObj).should.be.deepEqual ['$date','$msg','$type','0','1','2']
      lastLogObj[0].should.be.equal 111
      lastLogObj[1].should.be.equal 222
      lastLogObj[2].should.be.equal 333

    it 'log with one object as arguments', ->
      util.log 'test', a:111, b:222, c:333
      keys(lastLogObj).should.be.deepEqual ['$date','$msg','$type','a','b','c']
      lastLogObj.a.should.be.equal 111
      lastLogObj.b.should.be.equal 222
      lastLogObj.c.should.be.equal 333

    it 'log with one object after a list of arguments', ->
      util.log 'test', 111, 222, a:333, b:444
      keys(lastLogObj).should.be.deepEqual ['$date','$msg','$type','0','1','a','b']
      lastLogObj[0].should.be.equal 111
      lastLogObj[1].should.be.equal 222
      lastLogObj.a.should.be.equal 333
      lastLogObj.b.should.be.equal 444

    it 'log with message and error object', ->
      try
        do noFunc
      catch err
        util.log 'some error', err
        keys(lastLogObj).should.be.deepEqual ['$date','$msg','$type','error']
        lastLogObj.$type.should.be.equal 'info'
        lastLogObj.$msg.should.be.equal 'some error'
        lastLogObj.error.type.should.be.equal 'ReferenceError'
        lastLogObj.error.errorMessage.should.be.equal 'noFunc is not defined'
        lastLogObj.error.stack.length.should.be.greaterThan 1
        util.log.error 'some error again', err
        lastLogObj.$type.should.be.equal 'error'
        lastLogObj.$msg.should.be.equal 'some error again'
        lastLogObj.error.type.should.be.equal 'ReferenceError'

    it 'log only with error object', ->
      try
        do noFunc
      catch err
        util.log err
        keys(lastLogObj).should.be.deepEqual ['$date','$msg','$type','error']
        lastLogObj.$msg.should.be.equal 'noFunc is not defined'
        lastLogObj.error.type.should.be.equal 'ReferenceError'

    it 'log with message, args and error object', ->
      try
        do noFunc
      catch err
        util.log 'more error1', 'ups', err
        lastLogObj.$msg.should.be.equal 'more error1'
        lastLogObj[0].should.be.equal 'ups'
        lastLogObj[1].type.should.be.equal 'ReferenceError'

    it 'log with message, args and an object containing error object', ->
      try
        do noFunc
      catch err
        util.log 'more error2', 'ups', err: err
        lastLogObj.$msg.should.be.equal 'more error2'
        lastLogObj[0].should.be.equal 'ups'
        lastLogObj.err.type.should.be.equal 'ReferenceError'

    it 'log only with user created error object', ->
      util.log new Error 'another error'
      lastLogObj.$type.should.be.equal 'info'
      lastLogObj.$msg.should.be.equal 'another error'
      lastLogObj.error.type.should.be.equal 'Error'
      lastLogObj.error.errorMessage.should.be.equal 'another error'
      lastLogObj.error.stack.length.should.be.greaterThan 1
      util.log.error new Error 'another error again'
      lastLogObj.$type.should.be.equal 'error'
      lastLogObj.$msg.should.be.equal 'another error again'
      lastLogObj.error.type.should.be.equal 'Error'

  describe 'randomizer', ->
    # test random results may fail sometimes. If it fails, run again to be sure.

    it 'do not repeat random selection from array', ->
      list = [ 11, 22, 33, 44 ]
      result = ( util.rand list for i in [1..4] )
      result.should.be.not.deepEqual list
      result.sort().should.be.deepEqual list

    it 'random selection from parameter list', ->
      result = []
      result.push util.rand 111, 222, 333
      result.push util.rand 111, 222, 333
      result.push util.rand 111, 222, 333
      result.sort().should.be.deepEqual [111, 222, 333]

    it 'randomly selects a name from a "FromUser" object', ->
      result = (
        for i in [1..3]
          util.randFromName first_name:'AA', last_name:'BB', username:'CC'
      )
      result.sort().should.be.deepEqual ['AA', 'BB', 'CC']

    it 'selects a name from a incomplete "FromUser" object', ->
      result = ( util.randFromName first_name:'XX' for i in [1..3] )
      result.sort().should.be.deepEqual ['XX', 'XX', 'XX']
      result = ( util.randFromName username:'XX' for i in [1..3] )
      result.sort().should.be.deepEqual ['XX', 'XX', 'XX']

  describe 'formating', ->

    it 'Print integer numbers with two digits', ->
      util.dig2(0).should.be.equal '00'
      util.dig2(3).should.be.equal '03'
      util.dig2(9).should.be.equal '09'
      util.dig2(10).should.be.equal '10'
      util.dig2(22).should.be.equal '22'
      util.dig2(99).should.be.equal '99'
      util.dig2(111).should.be.equal '111'

    it 'print numbers with two decimal places', ->
      util.round2(0).should.be.equal '0.00'
      util.round2(1).should.be.equal '1.00'
      util.round2(0.1).should.be.equal '0.10'
      util.round2(88).should.be.equal '88.00'
      util.round2(88.8).should.be.equal '88.80'
      util.round2(8.08).should.be.equal '8.08'

  describe 'validating', ->

    it 'isWebURL', ->
      util.isWebURL('http://aaa.bbb').should.be.ok
      util.isWebURL('https://aaa.bbb').should.be.ok
      util.isWebURL('ftp://aaa.bbb').should.be.not.ok
      util.isWebURL('http://aaa.bbb/path/file.ext').should.be.ok

  describe 'text format conversion', ->

    describe 'html2txt', ->

      it 'convert tags to simple compatible text', ->
        util.html2txt('
          <h1>Title</h1>some text
          &amp; &lt;&#9786;&#x263a;&gt;
          <p>some paragraph</p>
          <ul><li>A apple</li><li>B banana</li><li>C cucumber</li></ul>
        ').should.be.equal '''Title

          some text & <☺☺>

          some paragraph

            • A apple
            • B banana
            • C cucumber'''

      it 'remove white edge chars', ->
        util.html2txt(' text\r\n').should.be.equal 'text'
        util.html2txt(' <b>text</b>\r\n').should.be.equal 'text'
        util.html2txt(' <b>text\r\n</b>').should.be.equal 'text'
        util.html2txt('<p>text</p>').should.be.equal 'text'

      it 'consider all white chars groups as one space', ->
        util.html2txt('some  text').should.be.equal 'some text'
        util.html2txt('some\n\ntext').should.be.equal 'some text'
        util.html2txt('some\r\ntext').should.be.equal 'some text'
        util.html2txt('some \ttext').should.be.equal 'some text'

      it 'show links', ->
        util.html2txt('ABC <a href="http://gnu.org">gnu</a> DEF')
            .should.be.equal 'ABC gnu (http://gnu.org) DEF'

    describe 'html2md', ->

      it 'convert tags to markdown', ->
        util.html2md('
          <h1>Title</h1>some text
          &amp; &lt;&#9786;&#x263a;&gt;
          <p>some <b>paragraph</b></p>
          <ul><li>A apple</li><li>B banana</li><li>C cucumber</li></ul>
        ').should.be.equal '''
          Title
          =====

          some text & <☺☺>

          some *paragraph*

            * A apple
            * B banana
            * C cucumber'''

      it 'ignore title inner tags', ->
        util.html2md('<h1>Some <b>Title</b></h1>').should.be.equal '''
        Some Title
        =========='''
        util.html2md('<h2>Some <b>Title</b></h2>').should.be.equal '''
        Some Title
        ----------'''
        util.html2md('<h3>Some <b>Title</b></h3>').should.be.equal '
        ### Some Title'
        util.html2md('<h4>Some <b>Title</b></h4>').should.be.equal '
        #### Some Title'
        util.html2md('<h5>Some <b>Title</b></h5>').should.be.equal '
        ##### Some Title'
        util.html2md('<h6>Some <b>Title</b></h6>').should.be.equal '
        ###### Some Title'

      it 'do not mix titles', ->
        util.html2md('<h1>Title 1</h1>\n<h1>Title 2</h1>').should.be.equal '''
        Title 1
        =======

        Title 2
        ======='''
        util.html2md('<h2>Title 1</h2>\n<h2>Title 2</h2>').should.be.equal '''
        Title 1
        -------

        Title 2
        -------'''

      it 'convert links to markdown', ->
        util.html2md('The <a href="http://gnu.org">gnu</a> project.')
            .should.be.equal 'The [gnu](http://gnu.org) project.'

      it 'ignores tag attributes', ->
        util.html2md('The <a href="http://gnu.org" x="123">gnu</a> project.')
            .should.be.equal 'The [gnu](http://gnu.org) project.'
        util.html2md('The <a x="123" href="http://gnu.org">gnu</a> project.')
            .should.be.equal 'The [gnu](http://gnu.org) project.'
        util.html2md('The <a aa="11" href="http://gnu.org" bb="22">gnu</a> project.')
            .should.be.equal 'The [gnu](http://gnu.org) project.'
        util.html2md('
          <h1 id="title">Title</h1>
          <div class="abc">some text</div>
          <p class="def">some <b class="ghi">paragraph</b></p>
          <ul class="list"><li class="item">A apple</li><li>B banana</li></ul>
        ').should.be.equal '''
          Title
          =====

          some text

          some *paragraph*

            * A apple
            * B banana'''

    describe 'simpleHTML', ->

      it 'simplify html', ->
        util.simpleHTML('<hr>
          <h1 id="title">Title</h1>
          <div class="abc">The <a aa="11" href="http://gnu.org" bb="22">gnu</a> project.</div>
          <p class="def"><i class="ghi">some</i> <b class="jkl">paragraph</b></p>
          <ul class="list"><li class="item">A apple</li><li>B banana</li></ul>
        ').should.be.equal '''⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯

        Title

        The <a href="http://gnu.org">gnu</a> project.

        <i>some</i> <b>paragraph</b>

          • A apple
          • B banana
        '''

      it 'simplify html without HR', ->
        util.simpleHTML '<hr>some <b>text</b>', removeHR: on
            .should.be.equal 'some <b>text</b>'
        util.simpleHTML 'some<hr><b>text</b>', removeHR: on
            .should.be.equal 'some\n<b>text</b>'

      it 'simplify links', ->
        util.simpleHTML('
          <a aa="11" href="http://gnu.org" bb="22">gnu</a>
          <a xx="123" href="http://gnu.org">gnu</a>
          <a href="http://gnu.org" xx="123">gnu</a>
        ').should.be.equal '
          <a href="http://gnu.org">gnu</a>
          <a href="http://gnu.org">gnu</a>
          <a href="http://gnu.org">gnu</a>
        '

      it 'remove nested tags', ->
        util.simpleHTML('a<b>b<i>c</i>d</b>e').should.be.equal 'a<b>bcd</b>e'
        util.simpleHTML('a<b>b<b>c</b>d</b>e').should.be.equal 'a<b>bcd</b>e'
        util.simpleHTML('a<b>b<b></b>c</b>d').should.be.equal 'a<b>bc</b>d'
        util.simpleHTML('a<b><b>b</b></b>c').should.be.equal 'a<b>b</b>c'
        util.simpleHTML('a<b><b></b></b>b').should.be.equal 'a<b></b>b'
        util.simpleHTML('a<b>b<i>c</i>d<i>e</i>f</b>g').should.be.equal 'a<b>bcdef</b>g'
        util.simpleHTML('a<b>b<b>c</b>d<b>e</b>f</b>g').should.be.equal 'a<b>bcdef</b>g'
        util.simpleHTML('a<b>b<i>c<i>d</i>e</i>f</b>g').should.be.equal 'a<b>bcdef</b>g'
        util.simpleHTML('a<b>b<b>c<b>d</b>e</b>f</b>g').should.be.equal 'a<b>bcdef</b>g'

      it 'remove unclosed tags', ->
        util.simpleHTML('a<i>b</i>c<b>d').should.be.equal 'a<i>b</i>cd'
        util.simpleHTML('a<b>b</b>c<b>d').should.be.equal 'a<b>b</b>cd'

      it 'remove unopened tags', ->
        util.simpleHTML('a<i>b</i>c</b>d').should.be.equal 'a<i>b</i>cd'
        util.simpleHTML('a<b>b</b>c</b>d').should.be.equal 'a<b>b</b>cd'
        util.simpleHTML('a</b>b<b>c</b>d').should.be.equal 'ab<b>c</b>d'

      it 'remove broken tags', ->
        util.simpleHTML('a<i>b</i>c<b').should.be.equal 'a<i>b</i>c'
        util.simpleHTML('a<i>x<b</i>c').should.be.equal 'a<i>x</i>c'
        util.simpleHTML('a<i>x<b</i>c<b').should.be.equal 'a<i>x</i>c'
        util.simpleHTML('a><i>b</i>c').should.be.equal 'a&gt;<i>b</i>c'
        util.simpleHTML('a<i>b></i>c').should.be.equal 'a<i>b&gt;</i>c'
        util.simpleHTML('a><i>b></i>c>').should.be.equal 'a&gt;<i>b&gt;</i>c&gt;'

      it 'remove mixed fails at once', ->
        util.simpleHTML('a><i>b</i>c<d').should.be.equal 'a&gt;<i>b</i>c'

  describe 'escapes', ->

    it 'escapeHTML', ->
      util.escapeHTML 'A <b>bold</b> "word" & end'
        .should.be.equal 'A &lt;b&gt;bold&lt;/b&gt; &quot;word&quot; &amp; end'

    it 'removeMD', ->
      util.removeMD 'A *bold* `word` _end_'
        .should.be.equal 'A bold word end'

    it 'escapeRE', ->
      util.escapeRE('[]|().a?').should.be.equal '\\[\\]\\|\\(\\)\\.a\\?'
      '['.should.match new RegExp util.escapeRE '['
      'a(b'.should.match new RegExp util.escapeRE 'a(b'
      'a(b'.should.not.match new RegExp util.escapeRE '.(.'
      '.(.'.should.match new RegExp util.escapeRE '.(.'

  describe 'captalize', ->

    it 'a word', ->
      util.captalize('word').should.be.equal 'Word'
      util.captalize('word ').should.be.equal 'Word '
      util.captalize(' word').should.be.equal ' Word'
      util.captalize('(word)').should.be.equal '(Word)'
      util.captalize('1word').should.be.equal '1word'

    it 'an empty value', ->
      util.captalize('').should.be.equal ''
      util.captalize([]).should.be.equal ''
      util.captalize(null).should.be.equal ''
      util.captalize().should.be.equal ''

    it 'a phrase', ->
      util.captalize('some title').should.be.equal 'Some Title'
      util.captalize('some nice title').should.be.equal 'Some Nice Title'
      util.captalize('some-nice-title').should.be.equal 'Some-Nice-Title'
      util.captalize('-some-nice-title-').should.be.equal '-Some-Nice-Title-'
      util.captalize('some_nice_title').should.be.equal 'Some_Nice_Title'

    it 'latin words', ->
      util.captalize('é uma ação rápida').should.be.equal 'É Uma Ação Rápida'
      util.captalize('açaí guardião ímã').should.be.equal 'Açaí Guardião Ímã'

