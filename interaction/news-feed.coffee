# News feed reader

{ log:logger, rand, randFromName, removeMD, timeout, interval } = require '../lib/util'

catchPrev = '([^\\s]+\\s)?\\s*'
catchPost = '\\s*(\\s[^\\s]+)?'
addWords  = '(?:adicion[ea]|guarde|registre|add)'
cmdAdd    = "(?:(?:feed|rss)\\s+#{addWords}" +
            "|#{addWords}(?:\\s+(?:a|o|the))?\\s+(?:feed|rss))" +
            '\\s+([^\\s]+)' +  # This group catches the feed url
            '(?:\\s+(?:e|and)\\s+)?(mostre(?:\\s+o)?\\s+t[ií]tulo|show(?:\\s+the)?\\s+title)?'
cmdRemove = '(?:(?:feed|rss)\\s+rm(?:remova|apague|rm)\\s+(?:o\\s+)?(?:feed|rss))\\s+([^\\s]+)'
listWords = '(?:liste|listar|list|ls)'
cmdList   = "(?:(?:feed|rss)\\s+#{listWords}" +
            "|#{listWords}(?:\\s+(?:a|o|the)s?)?\\s+(?:feeds?|rss))"
cmdShow   = '(?:(?:feed|rss)\\s+show\\s+([0-9]+)(?:\\s+de|\\s+from)?\\s+([^\\s]+)' +
            '|(?:mostr[ea]r?|leia|show)\\s+(?:[oa]s?\\s+)?([0-9]+)' +
            '(?:\\s*(?:primeira|not[ií]cia)s?|news)\\s+(?:do\\s+)(?:feed|rss)' +
            ')\\s+([^\\s]+)'
cmdHelp   = '(?:(?:feed|rss)\\s+help' +
            '|(?:me\\s+)?(?:ajud[ea]|help)\\s+(?:sobre\\s+)?(?:o\\s+)?(?:feed|rss))'
cmdUndef  = '^\\s*(?:feed|rss)\\s*$'

exports.feedRelatedRE = ///#{catchPrev}
                          (#{cmdAdd}|#{cmdRemove}|#{cmdList}|#{cmdShow}
                          |#{cmdHelp}|#{cmdUndef})#{catchPost}///i
exports.feedAddRE     = ///#{cmdAdd}///i
exports.feedRemoveRE  = ///#{cmdRemove}///i
exports.feedListRE    = ///#{cmdList}///i
exports.feedShowRE    = ///#{cmdShow}///i
exports.feedHelpRE    = ///#{cmdHelp}///i
exports.feedUndefRE   = ///#{cmdUndef}///i

instances = {}

exports.init = (bot)->
  instances[bot.username] = new NewsFeed bot
  timeout 60*5, -> do instances[bot.username].getFeeds
  # Init Feed Memory
  #feedMem[bot.username] = bot.memory.readJSON 'feed-reader'

exports.listen = (msgCtx)->
  instances[msgCtx.bot.username].listen msgCtx


# # # Feed Reader Feature # # # # # # # # # # # # # # # # # # # # # # # # # # #

exports.NewsFeed = class NewsFeed

  constructor: (@bot)->
    @memo = @bot.memory.readJSON 'feed-reader'
    @rssKnowHow = {}

  #botFeedMem = (bot)-> feedMem[bot.username]
  chatFeedMem: (msgCtx)-> @memo[msgCtx.chatCtx.chatID] ?= {} 
  saveFeedMem: -> @bot.memory.writeJSON 'feed-reader', @memo

  acceptableRequest: (msgCtx)->
    return false unless match = msgCtx.text.match exports.feedRelatedRE
    # Talk with me OR talk no other word but feed commands
    msgCtx.toMe or ((match?[1] and match?[match.length-1]) is '')

  listen: (msgCtx)->

    #if match = msgCtx.text.match exports.feedRelatedRE
    #  return unless @acceptableRequest msgCtx, match
      return unless @acceptableRequest msgCtx

      if match = msgCtx.text.match exports.feedUndefRE
        if @rssKnowHow[msgCtx.chatCtx.chatID] # I send help to this site recently
          msgCtx.answer "#{rand 'Oi', 'Olá', ''} #{randFromName msgCtx.from},
                         você precisa definir uma ação.
                         Se precisar, peça ajuda escrevendo \"@#{@username} feed help\"."
        else # The people may forgot the help
          msgCtx.answerMD "#{rand 'Oi', 'Olá', ''} #{removeMD randFromName msgCtx.from},
                           você precisa definir uma ação. #{do @help}."
          @rssKnowHow[msgCtx.chatCtx.chatID] = true
          timeout 60*60*4, -> @rssKnowHow[msgCtx.chatCtx.chatID] = false
      else if match = msgCtx.text.match exports.feedAddRE
        exports.addFeed msgCtx, match[2], match[3], from
      else if match = msgCtx.text.match exports.feedShowRE
        exports.showFeed msgCtx, match[2] or match[4], match[3] or match[5], from
      else if match = msgCtx.text.match exports.feedRemoveRE
        exports.removeFeed msgCtx, match[2], from
      else if match = msgCtx.text.match exports.feedListRE
        exports.listFeeds msgCtx
      else if match = msgCtx.text.match exports.feedHelpRE
        msgCtx.answerMD "É simples...\n\n#{do @help}"
      else
        msgCtx.answer "#{randFromName msgCtx.from} não entendi o seu comando.
                       Se precisar, peça ajuda escrevendo \"@#{@username} feed help\"."

  help: ->
    "Você pode agir como um programador e escrever:
    \n `[@#{removeMD @bot.username}] (feed|rss) <cmd>`
    \n Onde `<cmd>` pode ser `add`, `show`, `rm` ou `ls`.
    \n Após os comandos `add` e `rm` você deve colocar uma URL.
    \n Após o comando `show` coloque a quantidade e parte da URL.
    \n\nOu você pode agir como uma pessoa normal e escrever:
    \n `Adicione o rss http://bla.bla/bla`
    \n `bot, mostre 5 notícias do feed bla`
    \n `bot registre o feed http://bla.bla/bla`
    \n `Apague o feed http://bla.bla/bla`
    \n `@#{removeMD @bot.username}, liste os feeds`
    \n\n \"RSS\" e \"Feed\" são sinônimos para mim,
    assim como \"adicione\" e \"registre\".
    E me chamar pelo nome é opcional. 😉"

  listFeeds: (msgCtx)->
    list = ('▶ ' + url for url of @chatFeedMem(msgCtx)).join '\n'
    if lista is ''
      sendMD chatID, "Não tenho feeds registrados neste chat.
                      \nRegistre um com: `feed add <URL>`"
    else
      sendHTML chatID, "Feeds registrados:\n#{list}"

  addFeed: (msgCtx, url, showTitle, from)->
    showTitle = !!showTitle
    chatMemo = @chatFeedMem msgCtx
    if isWebURL url
      if chatMemo[url]?
        sendMsg chatID, "O feed #{url} já estava registrado."
      else
        chatMemo[url] = lastItemDate: 0, showTitle: showTitle
        showTitleInfo = if showTitle then 'mostrará o titulo do feed' \
                                     else 'mostrará o preview via Telegram'
        sendMsg chatID, "Feed #{url} registrado e #{showTitleInfo}."
        getFeed chatID, url, chatMemo[url], 1, (err)=>
          if err
            @removeFeed chatID, url
          else
            do @saveFeedMem
    else
      sendMsg chatID, "Ei #{randFromName from}! \"#{url}\" não é uma URL."

  showFeed = (msgCtx, num, matchStr, from)->
    num = parseInt num
    if num < 1
      return sendMsg chatID, "Zero notícias? Aff..."
    if num > 15
      sendMsg chatID, "#{num} notícias? Meio exagerado, não? Se tiver eu mostro."
    for url of @chatFeedMem msgCtx
      if 0 <= url.indexOf matchStr
        numStr = if num is 1 then 'uma notícia' else "as #{num} notícias"
        sendMsg chatID, "Vou buscar #{numStr} em #{url}"
        getFeed chatID, url, {}, num

  removeFeed: (msgCtx, url, from)->
    chatMemo = @chatFeedMem msgCtx
    unless chatMemo?
      return sendMsg chatID, "#{randFromName from}, você não registrou nenhum feed aqui."
    if isWebURL url
      if chatMemo[url]?
        delete chatMemo[url]
        do @saveFeedMem
        sendMsg chatID, "O feed #{url} foi removido."
      else
        sendHTML chatID, "O feed #{url} não está cadastrado.\nVeja a lista com <code>feed ls</code>"
    else
      sendMsg chatID, "Ei #{randFromName from}! \"#{url}\" não é uma URL."

  # Search for news each 30 min
  getFeeds: ->
    counter = 0
    for chatID of @memo
      timeout counter*10, do (id=chatID)=> =>
        logger "getFeeds para o chat #{id}..."
        @getFeedsFromChat id
      counter++
    timeout counter*10, =>
      logger "getFeeds finished!"
      do @saveFeedMem
      @getFeedsTimoutID = timeout 60*30, exports.getFeeds bot

  getFeedsFromChat: (msgCtx)->
    feedURLs = @chatFeedMem msgCtx
    counter = 0
    for feedURL,feedConf of feedURLs
      timeout counter*2, do (url=feedURL, conf=feedConf)=> =>
        logger "Pegando feed #{url} para o chat #{chatID}..."
        getFeed chatID, url, conf, 0
      counter++

  removeCDATA: (str)->
    if match = str.match /<!\[CDATA\[(.*)\]\]>/
      match[1]
    else
      str

  # Lê um feed RSS e manda as notícias para o chat e guarda a data da última,
  # maxNews = 0 mostra todas as novas
  # maxNews > 0 mostra as do topo
  getFeed: (msgCtx, url, conf, maxNews, callback=(->))->
    logger "get Feed #{url}"
    urlObj = parseURL url
    urlObj.headers = {
      'User-Agent': 'Mozilla like'
      'Accept-Language': 'pt-BR;q=1, pt;q=0.8, en;q=0.5'
    }
    urlObj.rejectUnauthorized = false
    protocol = if urlObj.protocol is 'https:' then https else http
    req = protocol.request urlObj, (res)=>
      if res.statusCode is 403
        sendMsg chatID, "Ups... Não tenho permissão para ler #{url} :-("
        return callback new Error "HTTP 403, Forbidden"
      data = ''
      res.on 'data', (chunk)-> data += chunk
      res.on 'end', =>
        # Mormalize XML text
        data = data.split(/[\r\n]+/).join('').replace(/\s+/g, ' ')
                   .replace(/<(item|entry)([ >])/g, '\n<ITEM$2')
                   .replace(/<\/(item|entry)>/g, '\n')
                   .split /\n+/
        if data.length < 2
          sendMsg chatID, "Hum? O feed #{url} não tem notícias. :-/" unless maxNews is 0
          return callback new Error "Empty feed"
        feedTitleMatch = data[0].match /<title[^>]*>(.*?)<\/title>/
        feedTitle = if feedTitleMatch? then feedTitleMatch[1] else '(feed sem título)'
        feedTitle = @removeCDATA(feedTitle)[0..50]
        counter = 0
        # Se maxNews = 0, deve-se mostrar todas as mais novas, então é preciso
        # percorrer o RSS de baixo para cima.
        # Se maxNews > 0, mostra só as N novas, de cima para baixo.
        logger "Feed #{url} tem #{data.length} linhas"
        readFlux = if maxNews is 0 then -1 else 1
        for line in data by readFlux
          if /^<ITEM[ >]/.test line and ( counter < maxNews and maxNews isnt 0 )
            try
              counter++ if @sendNewsItem line, counter, maxNews
              break if maxNews isnt 0 and counter >= maxNews
            catch err
              return callback err
        do callback
    req.on 'error', (err)->
      admDebug "Error getting feed #{url}\n\n#{err}"
      callback err
    do req.end

  sendNewsItem: (line, counter=0, maxNews=0)->
    {errorMsg, title, link, pubDate} = @getFeedItemData line
    if errorMsg?
      if maxNews > 0 # reuisição intencional de notícias ou registro do feed
        admDebug "Feed #{url} fail: #{errorMsg}"
        sendMsg chatID, "Ups... #{errorMsg}"
        throw Error errorMsg
    else
      pubTimestamp = pubDate.getTime()
      feedMemory = @chatFeedMem(msgCtx)[url]
      if pubTimestamp > feedMemory.lastItemDate or ( counter < maxNews and maxNews isnt 0 )
        if pubTimestamp > feedMemory.lastItemDate
          feedMemory.lastItemDate = pubTimestamp
          saveFeedMem msgCtx.bot
        logger "Enviando... #{link} -- #{pubDate}"
        timeout counter+1,
          do (num=counter+1, ft=feedTitle, t=title, l=link, d=pubDate)=> =>
            sendNewsCallback = (err)->
              if err
                admDebug "Message fail for feed #{url} -- #{pubDate}"
                logger 'Send feed news fail:', err
            if maxNews > 1
              sendHTML chatID, "<b>#{ft}</b> — #{d.getDatePtBR()},
                    #{dig2 d.getHours()}:#{dig2 d.getMinutes()}
                    \n#{num}º <a href=\"#{l}\">#{t}</a>",
                    disable_web_page_preview: true, sendNewsCallback
            else
              if conf.showTitle
                sendHTML chatID, "<b>#{ft}:</b> <a href=\"#{l}\">#{t}
                    — #{d.getDatePtBR()},
                    #{dig2 d.getHours()}:#{dig2 d.getMinutes()}</a>",
                    disable_web_page_preview: true, sendNewsCallback
              else
                sendHTML chatID, "<b>#{ft}</b> — <a href=\"#{l}\">#{d.getDatePtBR()},
                    #{dig2 d.getHours()}:#{dig2 d.getMinutes()}</a>",
                    sendNewsCallback
        true # news sended
      else
        false # nothing sended


  getFeedItemData: (feedItem)->
    data = errorMsg: null
    data.title = @removeCDATA feedItem.match(/<title[^>]*>(.*?)<\/title>/)[1]
    linkMatch = feedItem.match /<link[^>]*>(.*?)<\/link>/
    if linkMatch?
      data.link = @removeCDATA linkMatch[1]
    else
      linkMatch = feedItem.match /<link[^>]+href="([^"]+)"/
      if linkMatch?
        data.link = linkMatch[1]
      else
        data.errorMsg = "Não acho o link da notícia \"#{data.title}\"."
        logger "Não acho o link da notícia: #{feedItem}"
    pubDateMatch = feedItem.match /<pubDate[^>]*>(.*?)<\/pubDate>/
    unless pubDateMatch
      pubDateMatch = feedItem.match /<(?:[^>]+:)?date>(.*?)<\/(?:[^>]+:)?date>/
    unless pubDateMatch
      pubDateMatch = feedItem.match /<(?:[^>]+:)?published>(.*?)<\/(?:[^>]+:)?published>/
    unless pubDateMatch
      pubDateMatch = feedItem.match /<(?:[^>]+:)?updated>(.*?)<\/(?:[^>]+:)?updated>/
    unless pubDateMatch
      data.errorMsg = "A notícia \"#{data.title}\" não tem data."
      logger "Feed news has no pubDate:\n#{feedItem}"
    data.pubDate = new Date if pubDateMatch? then @removeCDATA pubDateMatch[1] else NaN
    if isNaN data.pubDate.getDay()
      invalidDate = if pubDateMatch? then pubDateMatch[1] else 'vazia'
      data.errorMsg = "A notícia \"#{data.title}\" tem uma data inválida. (#{invalidDate})"
    data

