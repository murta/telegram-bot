# Interact with Hello messages

translate = require '../lib/translate'
sendSpeak = require '../lib/send-speak'

exports.speakRE = /^\s*(fale|diga)[.,: ]+(que\s+)?(.+)$/i
exports.speakREtoMe = /\b(fale|diga)[.,: ]+(que\s+)?(.+)$/i

exports.listen = (msgCtx)->

  re = if msgCtx.toMe then exports.speakREtoMe else exports.speakRE
  if match = msgCtx.text.match re
    sendSpeak msgCtx.chatCtx, translate.badWords translate.pronoms match[3]

