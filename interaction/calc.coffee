# make calculations from aritimetc expressions on user messages

{rand} = require '../lib/util'

# A simpler RegExp to consume less proc on each `listen()` call
exports.basicMathRE = ///
  [√∛∜]\s*\(*\s*[-.0-9]+             |
  [-.0-9]+[-+\/*÷×%^√∛∜()\s]+[.0-9]+ |
  [-.0-9]+[⁰¹²³⁴⁵⁶⁷⁸⁹]+
///

operations = '([-+/*÷×%^]|\\*\\*|//)'
number = '\\(?-?[0-9]*\\.?[0-9]+\\)?'
simpleExp = "#{number}(\\s*#{operations}\\s*#{number})+"
exports.mathRE = ///
  (^|\s)
  (
    (
      -?[√∛∜] #{number}
    )|(
      #{number} [⁰¹²³⁴⁵⁶⁷⁸⁹]+
    )|(
      ( -?[√∛∜]? -?\(* \s* )* #{simpleExp} (\s*\))*
      (\s* [⁰¹²³⁴⁵⁶⁷⁸⁹]+)?
      (
        \s*#{operations}\s*
        ( -?[√∛∜]? -?\(* \s* )* ( #{number} | #{simpleExp} ) (\s*\))*
        (\s* [⁰¹²³⁴⁵⁶⁷⁸⁹]+)?
      )*
    )
  )
///i
exports.matchDate = /([0-9]{2}\/[0-9]{2}\/[0-9]{4}|[0-9]{2}\/20[0-9]{2})/

exports.translatePowToJS = (expression)->
  while (i = expression.indexOf '^') > 0
    left = i-1
    right = i+1
    right++ if expression[i+1] in ['+','-']
    if /[.0-9]/.test expression[i-1]
      left-- while /[.0-9]/.test expression[left-1]
    if /[.0-9]/.test expression[i+1]
      right++ while /[.0-9]/.test expression[right+1]
    if ')' is expression[i-1]
      opened = 1
      while opened > 0
        left--
        if expression[left] is ')' then opened++
        if expression[left] is '(' then opened--
        if left < 0
          throw Error "Bad structure before power"
    if /^[+-]?\(/.test expression[i+1..]
      right++ if expression[i+1] in ['+','-']
      opened = 1
      while opened > 0
        right++
        if expression[right] is '(' then opened++
        if expression[right] is ')' then opened--
        if right > expression.length
          throw Error "Bad structure after power"
    expression =
      ( if left is 0 then '' else expression[0..left-1] ) +
      "Math.pow(#{expression[left..right].replace('^',',')})" +
      expression[right+1..]
  expression

exports.translateRootToJS = (expression)->
  while (i = expression.match(/√|∛|∜/)?.index) >= 0
    right = i+1
    pow = '1/2' if expression[i] is '√'
    pow = '1/3' if expression[i] is '∛'
    pow = '1/4' if expression[i] is '∜'
    if /[-+.0-9]/.test expression[i+1]
      right++ while /[.0-9]/.test expression[right+1]
    if /^[+-]?\(/.test expression[i+1..i+2]
      right++ if expression[i+1] in ['+','-']
      opened = 1
      while opened > 0
        right++
        if expression[right] is '(' then opened++
        if expression[right] is ')' then opened--
        if right > expression.length
          throw Error "Bad root parenthesis structure"
    expression =
      ( if i is 0 then '' else expression[0..i-1] ) +
      "Math.pow(#{expression[i+1..right]},#{pow})" +
      expression[right+1..]
  expression

powZeroCode = '⁰'.charCodeAt 0
exports.translateToJS = (expression)->
  expression = expression
    .replace /\s/g, ''
    .replace /×/g,  '*'
    .replace /÷/g,  '/'
    .replace /\*\*/g, '^'
    .replace /[⁰¹²³⁴⁵⁶⁷⁸⁹]+/g, (pow)-> '^' + ( '⁰¹²³⁴⁵⁶⁷⁸⁹'.indexOf i for i in pow ).join ''
  exports.translateRootToJS exports.translatePowToJS expression

exports.evalMath = (expression)->
  val = eval exports.translateToJS expression
  if Number.isNaN val
    throw Error 'Invalid expression'
  else
    val

exports.listen = (msgCtx)->
  if exports.basicMathRE.test msgCtx.text
    match = msgCtx.text.match exports.mathRE
    if match? 
      expression = match[2].replace /\s/g,''
      unless exports.matchDate.test expression
        try
          result = exports.evalMath expression
          msgCtx.text = msgCtx.text.replace exports.mathRE, " #{result}"
        catch err
          result = rand ['Argh!', 'Ugh!', 'Ouch...', 'Hum?', 'Que?', 'WTF?']
          msgCtx.bot.admDebug "Math ERR: #{err.message}\nExp: #{expression}"
        mathMsg = "<code>#{expression}</code> = <code>#{result}</code>" +
          if err? then '\n' + err.message else ''
        msgCtx.chatCtx.sendHTML mathMsg

