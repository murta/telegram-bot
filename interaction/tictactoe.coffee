{log:logger, timeout, removeMD, rand, randFromName} = require '../lib/util'

exports.askForGameRE = /^\s*((vamos|quero) jogar )?((o|um|uma) )?(jogo da )?velha\s*[.!?]*\s*$/i

exports.listen = (msgCtx)->
  chatCtx = msgCtx.chatCtx
  game = new TicTacToe msgCtx
  if msgCtx.text.match exports.askForGameRE
    do game.init
    timeout .3, -> do game.randStart
  else if match = msgCtx.text.match /\b([abc][123])\b/i
    if match? and game.usrIsPlaying()
      game.humanPlay match[1]

exports.TicTacToe = class TicTacToe

  constructor: (@msgCtx)->
    @chatCtx = @msgCtx.chatCtx
    @from = @msgCtx.from
    @ctx = @chatCtx.get('tictactoe') or {}

  usrIsPlaying: -> @ctx[@from.id]?

  finish: -> @ctx[@from.id] = null

  init: ->
    if @usrIsPlaying()
      @msgCtx.answer removeMD(randFromName @from) +
                     ', nós ainda temos um jogo em andamento!'
      timeout .2, => do @draw
    else
      @gameCtx = @ctx[@from.id] = { set: [[0,0,0],[0,0,0],[0,0,0]] }
      @chatCtx.set 'tictactoe', @ctx
      @set = @gameCtx.set
      @msgCtx.answerMD rand(['Oi','Ok','Tá','Certo']) + ' ' +
                     removeMD(randFromName @from) +
                     rand(['.', '!']) + '\n
                     Para jogar apenas escreva o nome da casa juntando letra e número.
                     Por exemplo: `B2` é o centro; e `C1` é a quina direta no topo.
                     Eu sou ⚙ e você é ✗.'

  randStart: ->
    if Math.random() < .5
      @chatCtx.sendMsg 'Eu começo!'
      do @robotPlay
    timeout .3, => do @draw

  save: -> do @chatCtx.delayedSave

  chr: (val)-> if val is 0 then '·' else if val is -1 then '○' else '×'

  draw: ->
    @chatCtx.sendMD """
    \xA0\xA0\xA0 A \xA0 B \xA0 C
    1\xA0 `#{@chr @set[0][0]}` `#{@chr @set[0][1]}` `#{@chr @set[0][2]}`
    2\xA0 `#{@chr @set[1][0]}` `#{@chr @set[1][1]}` `#{@chr @set[1][2]}`
    3\xA0 `#{@chr @set[2][0]}` `#{@chr @set[2][1]}` `#{@chr @set[2][2]}`
    """

  robotPlay: ->
    x = -1; y = 0
    while @set[y][x] isnt 0
      x = Math.round Math.random() * 2
      y = Math.round Math.random() * 2
    @set[y][x] = -1
    do @save

  humanPlay: (place)->
    place = place.toUpperCase().split ''
    x = place[0].charCodeAt(0) - 'A'.charCodeAt(0)
    y = parseInt(place[1]) - 1
    if @set[y][x] is 0
      @set[y][x] = 1
      unless @testFinish()
        do @robotPlay
        unless @testFinish()
          do @draw
    else
      ops = rand ['Oi?', 'Ei!', 'Ops...', '']
      if @set[y][x] is -1
        @chatCtx.sendMsg ops + " Eu já #{rand ['marquei', 'joguei']} aí."
      else
        @chatCtx.sendMsg ops + " Você já #{rand ['marcou', 'jogou']} aí."
    do @save

  testWinner: ->
    tie = true
    humanWin = false
    robotWin = false
    for y in [0,1,2]
      # testa linhas horizontais
      humanWin = true if @set[y][0] is @set[y][1] is @set[y][2] is 1
      robotWin = true if @set[y][0] is @set[y][1] is @set[y][2] is -1
      for x in [0,1,2]
        # testa linhas verticais
        humanWin = true if @set[0][x] is @set[1][x] is @set[2][x] is 1
        robotWin = true if @set[0][x] is @set[1][x] is @set[2][x] is -1
        # Se uma casa estiver vazia ainda não deu velha
        tie = false if @set[y][x] is 0
    # Testar diagonais
    humanWin = true if @set[0][0] is @set[1][1] is @set[2][2] is 1
    humanWin = true if @set[0][2] is @set[1][1] is @set[2][0] is 1
    robotWin = true if @set[0][0] is @set[1][1] is @set[2][2] is -1
    robotWin = true if @set[0][2] is @set[1][1] is @set[2][0] is -1
    return 'TIE'   if tie
    return 'HUMAN' if humanWin
    return 'BOT'   if robotWin
    false

  testFinish: ->
    name = randFromName @from
    winResult = do @testWinner
    if winResult
      do @draw
      do @finish
      if winResult is 'HUMAN'
        @chatCtx.sendMsg "#{name} #{rand ['ganhou', 'me venceu']}!"
      if winResult is 'BOT'
        @chatCtx.sendMsg "#{rand ['Ganhei de', 'Venci']} #{name}!"
      if winResult is 'TIE'
        @chatCtx.sendMsg "#{rand ['Droga!','WTF!','']} Deu velha#{rand ['!', '.', '...']}"
      return true
    else
      return false

