# Searches on Google Maps API and returns a localization, to the client display a map.

wikipedia = require '../lib/wikipedia'
{log:logger} = require '../lib/util'
{parse:parseURL} = require 'url'
https = require 'https'

exports.whereRE = /onde\s+(fica|está|é|eh)\s*(o|a)?\s+([^?!.]+)([?!.])?/i

exports.listen = (msgCtx)->
  query = msgCtx.text.match(exports.whereRE)?[3]
  if query?
    exports.findPlace query, (err, location)->
      if err?
        logger 'GPS fail:', err
        if err.status is 'ZERO_RESULTS'
          msgCtx.answer "Não consegui achar #{query}. :-(\n" +
                        "Isso é mesmo um endereço?"
        else
          msgCtx.answer "Não consegui achar. :-(\n#{err}"
      else
        msgCtx.answerLocation location.lat, location.lng, (err, data)->
          if err
            msgCtx.bot.admDebug "findPlace (#{query}) fail: #{err}"

# Acessa a API do Google Maps que converte endereços em latitude e longitude.
exports.findPlace = (query, callback=(->))->
  query = query.replace /["'!?]/g, ''
  logger "Localizando onde fica \"#{query}\"..."
  url = parseURL 'https://maps.googleapis.com/maps/api/geocode/json?' +
                 'sensor=false&address=' + encodeURIComponent query
  url.headers = {
    'User-Agent': 'Mozilla like'
    'Accept-Language': 'pt-BR;q=1, pt;q=0.8, en;q=0.5'
  }
  req = https.request url, (res)->
    data = ''
    res.on 'data', (chunk)-> data += chunk
    res.on 'end', ->
      try
        data = JSON.parse data
      catch err
        return callback err
      if data.results?[0]?.geometry?.location?.lat?
        callback null, data.results[0].geometry.location
      else
        err = new Error "Cant find location for #{query}. Status: #{data.status}"
        err.status = data.status
        callback err
  req.on 'error', (err)-> callback err
  do req.end

