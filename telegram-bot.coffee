#!/usr/bin/env coffee
# telegram-bot - an interactive chat bot for Telegram platform
# Copyright (C) 2016 Aurélio A. Heckert <aurium(a)colivre.coop.br>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

{
  log:logger, rand, randFromName, timeout, interval, dig2, isWebURL, mkID,
  html2txt, html2md, simpleHTML, escapeHTML, removeMD, inspect
} = require './lib/util'
fs = require 'fs'
api = require './lib/telegram-api'
ChatContext = require './lib/chat-context'
MessageContext = require './lib/message-context'

# Match ISO Date.
isoDateRE = '([0-9]{4}-[0-9]{2}-[0-9]{2})T?([0-9]{2}:[0-9]{2}(?::[0-9]{2})?)?'


# Envia um localização geográfica e o cliente Telegram mostra no mapa.
sendLocation = (chat, lat, lon, callback)->
  api 'sendLocation', {chat_id:chat, latitude:lat, longitude:lon}, callback

# Creates a Teleram bot instance
# `config` may have this keys:
# * `name`: the friendly bot's name
# * `username`: the @ reference for this bot
# * `token`: the API token for this bot
# * `admChatID`: the chat where this bot can send warnings.
# * `configDir`: the directory where this bot can register states between initializations.
module.exports = class Bot

  constructor: (config={})->
    @name = config.name
    @username = config.username
    @token = config.token
    @admChatID = config.admChatID
    @srcRepo = (config.srcRepo or '').toString().replace(/\s/g,'') or 'No... Hiding.'
    @memory = require('./lib/memo') config.configDir
    @admDebug = api.mkAdmDebug this
    @lastUpdate = parseInt @memory.readFile 'lastUpdate', 0
    @__chatCtxCache = {}
    @__interactions = []
    @admDebug "I am ALIVE #{new Date}"

  setLastUpdate: (@lastUpdate)-> @memory.writeFile 'lastUpdate', @lastUpdate

  getUpdates: (opts={}, callback)->
    api.getUpdates this, opts, callback

  stopUpdate: ->
    clearTimeout @__updateTimeout if @__updateTimeout

  addInteraction: (modules...)->
    for m in modules
      @__interactions.push m
      m.init? this

  loadDefaultInteractions: (callback=(->))->
    fs.readdir "#{__dirname}/interaction", (err, files)=>
      return callback err if err
      for f in files when /\.coffee$/.test f
        @addInteraction require './interaction/' + f[0..-8]
      do callback

  getChatContext: (chatID)-> @__chatCtxCache[chatID] ?= new ChatContext this, chatID

  # responde o que ouve por reconhecimento de padrão, ou roteia em casos mais elaborados
  reactToText: (message)->
    chatID = message.chat.id
    chatCtx = @getChatContext chatID

    msgCtx = new MessageContext message, chatCtx

    if interactionModule = chatCtx.lockContext?.module
      # Directly send the message to the module who lock the talk context.
      cmd = interactionModule.listen msgCtx
      chatCtx.computeCommand chatCtx, cmd, interactionModule
    else
      for interactionModule in @__interactions
        cmd = interactionModule.listen msgCtx
        chatCtx.computeCommand chatCtx, cmd, interactionModule
        break if cmd?.lockContext

    from = message.from
    #for textLine in message.text.split /[\r\n]+/
    textLine = message.text
    logger "Listen at #{chatID} from #{from.username}:", usrMsg: textLine

    helpMsg = '
    O meu objetivo é interagir de forma natural, mas sem causar floods em grupos.
    \n
    \nEu posso:
    \nResponder a "*o que é [alguma coisa]?*" com uma busca na Wikipedia e DDG.
    \nResponder a "*onde fica [algum lugar]?*" com uma busca no Google Maps.
    \nCalcular expressões algébricas nas frases, tipo "*tenho (38+52)/6 bananas*".
    \nGerar áudio do texto que você quiser após a palavra "*diga*".
    \nResponder a "*que horas são?*" de uma forma humana.
    \nJogar velha. (é só me pedir!)
    \nTe trazer notícias. Escreva "*rss help*" para saber mais.
    \nGerir suas tarefas. Escreva "*todo help*" para saber mais.
    \n
    \nSe precisar, fale com @Aurium.
    '
    # default commands
    if textLine.match ///^\s*@?(#{@username}|bot)?\/start@?(#{@username}|bot)?///i
      sendMD chatID, "Olá #{removeMD randFromName from}!\n" + helpMsg
    if textLine.match ///^\s*@?(#{@username}|bot)?\/help@?(#{@username}|bot)?///i
      sendMD chatID, helpMsg

    # Tem Gente
    if textLine.match /.*(algu[eé]m|tem\s*gente)\s+no\s+Raul(HC)?\?\s*$/i
      reactTemGenteNoRaul chatID

    # todo.txt
    if match = textLine.match ///(?:^(?:\s*bot[:,]?)?|@?#{@username})\s*(todo\s*[:;,]?)\s*(.*)$///i
      todoCall = match[1]
      todoCMD = match[2].replace /^\s*|\s*$/g, ''
      if match = todoCMD.match /^(lista?r?|ls)$/i
        if todoMem = todo.mem[chatID]?.todo
          list = ("<b>#{i}:</b> #{todo.getText chatID, i}" for t,i in todoMem).join '\n'
          if list is ''
            sendMD chatID, "Não tenho tarefas registrados neste chat.
                            \nRegistre um com: `todo <texto da tarefa>`"
          else
            sendHTML chatID, "Tarefas registradas:\n#{list}", disable_web_page_preview: true
        else
          sendMsg chatID, 'Não existem tarefas registradas para este chat.'
      else if match = todoCMD.match /^([0-9]+)\s+(done|(foi|est[aá]|t[aá])?\s*feita)$/i
        todo.done chatID, match[1]
      else if match = todoCMD.match /^(done|close|feita|fechar|feche)\s+([0-9]+)$/i
        todo.done chatID, match[2]
      else if match = todoCMD.match /^(update|atualizar?|change)\s+([0-9]+)\s+([^\s]+)\s+(.+)$/i
        todo.update chatID, match[2], match[3], match[4]
      else if match = todoCMD.match /^download$/i
        todo.download chatID
      else if match = todoCMD.match /^(help|ajuda)$/i
        exampleDate = (new Date).toISOString().replace /\..*$/, ''
        sendHTML chatID, """
          Eu posso gerenciar tarefas no modelo <a href="http://todotxt.com">todo.txt</a>
          Os comandos são:
          • <code>todo list</code> mostra as tarefas registradas neste canal;
          • <code>todo done &lt;num&gt;</code> marca a tarefa como concluída;
          • <code>todo update &lt;num&gt; &lt;propriedade&gt; &lt;valor&gt;</code> modifica uma tarefa;
          • <code>todo download</code> envia o arquivo todo.txt deste canal;
          • <code>todo: &lt;algum texto&gt;</code> registra uma nova tarefa.

          Tarefas podem ter um <i>deadline</i>. Esta deve ser a última informação na tarefa e deve ser escrita no formato ISO.
          Exemplo: #{exampleDate}
          """, disable_web_page_preview: true
      else if /todo:/i.test todoCall
        todo.add chatID, from, todoCMD
      return # block text catch by next matches


reactTemGenteNoRaul = (chatID)->
  url = parseURL 'http://raulhc.cc/bin/tem-gente?type=json'
  url.headers = {
    'User-Agent': 'Mozilla like'
    'Accept-Language': 'pt-BR;q=1, pt;q=0.8, en;q=0.5'
  }
  req = http.request url, (res)->
    data = ''
    res.on 'data', (chunk)-> data += chunk
    res.on 'end', ->
      try
        data = JSON.parse data
      catch err
        logger 'TemGenteNoRaul Fail', err, 'Data:', data
        admDebug "TemGenteNoRaul Fail #{err}"
        return sendMsg chatID, "Não consegui ver se tem gente no RaulHC."
      numPess = data.numPess
      if numPess is 0
        # Example { "numPess": 0, "fechaEm": null }
        sendMsg chatID, "Parece que não tem gente no RaulHC agora."
      else
        # Example { "numPess": 1, "fechaEm": "2016-03-13T01:06:29" }
        pessTxt = if numPess is 1 then 'uma pessoa' else "#{numPess} pessoas"
        timeTxt = (new Date data.fechaEm).getTimePtBR()
        sendMsg chatID, "Tem #{pessTxt} no RaulHC até #{timeTxt}."
  req.on 'error', (err)->
    logger 'TemGenteNoRaul Fail', err
    admDebug "TemGenteNoRaul Fail #{err}"
    sendMsg chatID, "Não consegui ver se tem gente no RaulHC."
  do req.end


###
# todo.txt Feature # # # # # # # # # # # # # # # # # # # # # # # # #

todo = {}

# Memory
todo.mem = memory.readJSON 'todotxt'
todo.save = -> memory.writeJSON 'todotxt', @mem

# Registra uma tarefa
todo.add = (chatID, from, text)->
  text = text.replace /^\s*|\s*$/g, ''
  chatMem = @mem[chatID] ?= { todo:[], done:[] }
  # The priority must be the first information.
  if match = text.match /^\(([A-Z])\)/
    priority = match[1]
    text = text.replace /// ^\(.\)\s* ///, ''
  # The create date must be before the text.
  if match = text.match ///^(#{isoDateRE})///
    createdAt = match[1]
    text = text.replace ///^#{isoDateRE}\s+///, ''
  else
    createdAt = (new Date).toISOString().replace /\..*$/, ''
  # The deadline must be at the line end.
  if match = text.match ///\s(#{isoDateRE})$///
    deadline = match[1]
    text = text.replace ///\s+#{isoDateRE}$///, ''
  task = { text: text, priority: priority, createdAt: createdAt, deadline: deadline, from: from }
  todoList = chatMem.todo
  position = todoList.length
  for curTask,i in todoList
    if (task.priority or 'Z') <= (curTask.priority or 'Z')
      if task.createdAt < curTask.createdAt
        position = i
        break
  chatMem.todo = [ todoList[..position-1]..., task, todoList[position..]... ]
  do @save
  sendHTML chatID, "Tarefa \"#{@taskToText task}\" adicionada.", disable_web_page_preview: true

# Makes ISO Date more human
todo.formatDate = (isoDate)->
  mesPtBRshort = 'jan fev mar abr mai jun jul ago set out nov dez'.split ' '
  isoDate.replace /T/, ' '
         .replace /([0-9]+:[0-9]+):[0-9]+$/, '$1'
         .replace /-([0-9]+)-/, (sel,g1)-> "-#{mesPtBRshort[parseInt(g1) - 1]}-"

# Localiza uma tarefa e representa formatada com HTML
todo.getText = (chatID, index)->
  @taskToText @mem[chatID]?.todo[index]

# Representa uma tarefa, formatada com HTML
todo.taskToText = (task, opts={})->
  unless task?
    "A tarefa [#{index}] não existe."
  else
    text = ''
    if task.priority
      text += "(<b>#{task.priority}</b>) "
    if task.createdAt? and not opts.noDates
      text += "<code>#{@formatDate task.createdAt}</code> "
    text += escapeHTML task.text
    if task.deadline and not opts.noDates
      text += " <code>#{@formatDate task.deadline}</code>"
    text

# Atualiza propriedade da tarefa
todo.update = (chatID, index, property, value)->
  chatMem = @mem[chatID]
  index = parseInt index
  if chatMem
    todoList = chatMem.todo
    if todoList[index]
      unless property in ['priority', 'deadline']
        return sendMsg chatID, "A propriedade \"#{property}\"
                                não pode ser modificada.\n
                                As permitidas são: \"priority\" e \"deadline\"."
      if property is 'priority'
        unless /^[A-Z]$/.test value
          return sendMsg chatID, "Prioridade \"#{value}\" é Invalida.
                                  Use valores entre \"A\" e \"Z\"."
        todoList[index].priority = value
      if property is 'deadline'
        unless ///#{isoDateRE}///.test value
          exampleDate = (new Date).toISOString().replace /\..*$/, ''
          return sendMsg chatID, "Deadline \"#{value}\" é Invalida.
                                  Escreva a data em formato internacional.
                                  \nExemplo: #{exampleDate}"
        todoList[index].deadline = value
      do @save
      sendHTML chatID, "A tarefa <b>#{index}</b>
                        \"#{@getText chatID, index}\" foi atualizada.", disable_web_page_preview: true
    else
      sendMsg chatID, "A tarefa \"#{index}\" não existe."
  else
    sendMsg chatID, 'Não existem tarefas neste chat.'

# Fecha uma tarefa
todo.done = (chatID, index)->
  chatMem = @mem[chatID]
  index = parseInt index
  if chatMem
    todoList = chatMem.todo
    if todoList[index]
      taskText = @getText chatID, index
      task = todoList[index]
      task.closedAt = (new Date).toISOString().replace /\..*$/, ''
      chatMem.done.push task
      chatMem.todo = [ todoList.slice(0,index)..., todoList.slice(index+1)... ]
      do @save
      sendHTML chatID, "A tarefa \"#{taskText}\" foi finalizada.", disable_web_page_preview: true
    else
      sendMsg chatID, "A tarefa \"#{index}\" não existe."
  else
    sendMsg chatID, 'Não existem tarefas neste chat.'

# Envia o arquivo todo.txt
todo.download = (chatID)->
  todoList = @mem[chatID]?.todo or []
  if todoList.length is 0
    return sendMsg chatID, 'Não existem tarefas nesse chat.'
  content = ''
  for task in todoList
    content += "(#{task.priority}) "  if task.priority?
    content += "#{task.createdAt} "   if task.createdAt?
    content += task.text
    content += " #{task.deadline}"    if task.deadline?
    content += '\n'
  api 'sendDocument', file: content, fileName: 'todo.txt', chat_id: chatID, (err, data)->
    if err?
      logger 'send todo.txt ERR:', err, 'return:', data
      admDebug "send todo.txt Fail for \"#{chatID}\"\n#{err}"
      sendMsg 'Ups... Não foi possível enviar o arquivo todo.txt'

# Notifica tarefas próximas ao deadline
interval 60*30, ->
  now = new Date
  quarterHour = 15 * 60 * 1000
  halfHour = quarterHour * 2
  oneHour = quarterHour * 4
  oneDay = oneHour * 24
  for chatID, chatMem of todo.mem
    logger "Verificando tarefas para #{chatID}..."
    try
      for task in chatMem.todo
        if task.deadline?
          delta = (new Date task.deadline) - now
          if (oneDay-quarterHour) < delta < (oneDay+quarterHour)
            sendHTML chatID, "Falta um dia para realizar:
                              #{todo.taskToText task, noDates: true}", disable_web_page_preview: true
          if (oneHour-quarterHour) < delta < (oneHour+quarterHour)
            sendHTML chatID, "Está quase na hora!
                              #{todo.taskToText task, noDates: true}", disable_web_page_preview: true
    catch err
      admDebug "Notificar Deadline todo.txt fail.\n#{err}"
      logger 'Notificar Deadline todo.txt fail.', err
###

# if this is the main module, start a bot.
unless module.parent
  botName = process.env.bot_name
  botUserName = process.env.bot_username
  token = process.env.bot_token
  admChatID = process.env.bot_adm_chat_id
  unless botUserName? and token?
    console.error """
      Este script implementa um bot multifuncional para o Telegram sem usar
      nenhuma lib de abstração fora os módulos nativos do Node.js.

      Para executar esse script é preciso antes cadastrar um Bot no Telegram.
      https://core.telegram.org/bots#create-a-new-bot
      E então definir algumas variáveis de ambiente para configurar o bot.

      Exemplo:
      $ export bot_name="Aurium's Bot"
      $ export bot_username=AuriumBot
      $ export bot_token='1234567890:abcdefghijk-lmnopqrst_uwxyz'
      $ export bot_adm_chat_id=0987654321
      $ #{process.argv[1]}

      `bot_adm_chat_id` é o ID de uma conversa qualquer. Você pode iniciar esse
      script sem essa informação, iniciar uma conversa com o bot, e então coletar
      esse ID no log. Essa configuração permite que o bot lhe repasse informações
      como notificações de erro.
    """
    process.exit 1

  bot = new Bot \
    name:      botName,
    username:  botUserName,
    token:     token,
    admChatID: admChatID,
    configDir: process.env.HOME + '/.config/telegram-' + botUserName,
    srcRepo:   'https://gitlab.com/raulhc/telegram-bot/blob/master/telegram-bot.coffee'
  do bot.loadDefaultInteractions
  bot.getUpdates live:true, timeout:40, delay:4 # both in seconds

