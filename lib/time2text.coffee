weekPtBR = 'dom seg ter qua qui sex sáb'.split ' '
weekExPtBR = 'domingo segunda terça quarta quinta sexta sábado'.split ' '

monthPtBR = 'janeiro fevereiro março abril maio junho julho agosto
             setembro outubro novembro dezembro'.split ' '

hourNamePtBr = 'zero uma duas três quatro cinco seis sete oito nove dez onze'.split ' '

{dig2, captalize} = require './util'

module.exports = class Time2Text

  constructor: (locale)->
    @locale = Time2Text.buildLocaleArray locale

  getDayName: (date, extensive=true)->
    if extensive then weekExPtBR[date.getDay()] else weekPtBR[date.getDay()]

  getDate: (date, withWeek=true, withYear=false)->
    day = if withWeek then "#{@getDayName date}, " else ''
    year = if withYear then " de #{date.getFullYear()}" else ''
    "#{day}#{date.getDate()} de #{monthPtBR[date.getMonth()]}#{year}"

  # Makes an HTML formated calendar
  # `dayMarks` is a hash of `day: "<url>"`
  mkCalendar: (month, year=(new Date).getFullYear(), dayMarks={})->
    cal = "<b> #{captalize monthPtBR[month]} de #{year}</b>\n"
    day = new Date year, month, 1
    if day.getDay() > 0
      cal +=  ' xx |' for i in [ 0 .. day.getDay()-1 ]
    while day.getMonth() is month
      monthDay = day.getDate()
      cal += ' ' + (
        if (url = dayMarks[monthDay])?
          "<a href=\"#{url}\">#{dig2 monthDay}</a>"
        else
          dig2 monthDay
      ) + ' '
      cal += if day.getDay() is 6 then '\n' else '|'
      day.setDate day.getDate() + 1
    if day.getDay() > 0
      for i in [ day.getDay() .. 6 ]
        cal +=  ' xx '
        cal +=  '|' if i < 6
    cal.replace /\s*$/g, ''

  getTime: (date)->
    h = date.getHours()
    m = date.getMinutes()
    hh = if h < 12 then h else h-12

    if h is 12
      return if m < 20
        "meio dia"
      else if m < 40
        "meio dia e meia"
      else
        "quase uma da tarde"

    if h is 0
      return if m < 20
        "meia noite"
      else if m < 40
        "meia noite e meia"
      else
        "quase uma da madrugada"

    if m < 20
      "#{hourNamePtBr[hh]} da #{@getTurno h}"
    else if m < 40
      "#{hourNamePtBr[hh]} e meia da #{@getTurno h}"
    else
      if h is 11
        "quase meio dia"
      else if h is 23
        "quase meia noite"
      else
        "quase #{hourNamePtBr[hh+1]} da #{@getTurno h+1}"

  getTurno: (hour)->
    if       0 <= hour <= 6  then 'madrugada'
    else if  7 <= hour <= 11 then 'manhã'
    else if 12 <= hour <= 18 then 'tarde'
    else if 19 <= hour <= 23 then 'noite'

noRepeatPush = (arr, val)->
  if arr[arr.length-1] is val
    false
  else
    arr.push val
    true

Time2Text.buildLocaleArray = (localeSrc)->
  localeSrc ?= ['en']
  localeSrc = [localeSrc] if 'string' is typeof localeSrc
  locales = []
  for l in localeSrc
    l = l.toString().toLowerCase()
    noRepeatPush locales, l
    noRepeatPush locales, l.split('-')[0]

