{log:logger} = require './util'
{parse:parseURL} = require 'url'
https = require 'https'

# Busca na Wikipedia
# * `query`: é o texto que se espera encontrar
# * `langs`: um array de línguas onde a busca pode ser feita
exports.query = wikipediaQuery = (query, langs, callback=(->))->
  unless langs?.constructor is Array and langs.length > 0
    return callback new Error 'No langs array defined for Wikipedia search.'
  lang = do langs.shift
  lastLang = langs.length is 0
  query = query.replace(/["'.;:!?]/g, ' ').replace(/\s+/g, ' ').replace /^\s*|\s*$/g, ''
  cacheID = lang + '!' + query
  if wikipediaQuery.cache[cacheID]?
    logger "Wikipedia cached answer", lang:lang, query:query
    exports.extract wikipediaQuery.cache[cacheID], callback
  else
    logger "Quering Wikipedia...", query:query, lang:lang
    encQuery = encodeURIComponent query
    url = parseURL "https://#{lang}.wikipedia.org/w/api.php?action=query" +
                   '&converttitles=1&format=json&redirects=true' +
                   '&prop=pageimages|extracts&titles=' + encQuery
    url.headers = {
      'User-Agent': 'Mozilla like'
      'Accept-Language': 'pt-BR;q=1, pt;q=0.8, en;q=0.5'
    }
    req = https.request url, (res)->
      data = ''
      res.on 'data', (chunk)-> data += chunk
      res.on 'end', ->
        try
          data = JSON.parse data
        catch err
          logger.error 'Fail to parse Wikipedia answer', err, query:query, lang:lang
          if lastLang
            return callback err
          else
            return wikipediaQuery query, langs, callback
        wikipediaQuery.cache[cacheID] = data
        exports.extract data, query, lang, langs, callback
    req.on 'error', (err)->
      logger.error 'Fail query Wikipedia', err, query:query, lang:lang
      if lastLang
        callback err
      else
        wikipediaQuery query, langs, callback
    do req.end

wikipediaQuery.cache = {} # previne delay e alivia o site mais importante do mundo.

# Coleta a informação que interessa de um resultado de busca da Wikipedia.
exports.extract = (data, query, lang, langs, callback)->
  resp = { err: 'no result.' }
  if data.query?.pages?
    for id,page of data.query.pages
      logger 'Wikipedia page found.', id:id, query:query, lang:lang
      try
        if page.extract?
          resp = {
            url: "https://#{lang}.wikipedia.org/wiki/#{page.title.replace /\s/g, '_'}"
            title: page.title
            image: page.thumbnail?.source
            text: page.extract
            query: query
            lang: lang
          }
      catch err
        logger.error resp.err = "Wikipedia's data extraction fail: #{err.message}", err
  if resp.err is 'no result.' and langs.length > 0
    return wikipediaQuery query, langs, callback
  if resp.err
    resp.err = new Error resp.err
    resp.err.query = query
    resp.err.lang = lang
    callback resp.err
  else
    callback null, resp
