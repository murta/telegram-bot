fs = require 'fs'
{log} = require './util'

module.exports = memoBuilder = (storageDir)->
  fsPersist = storageDir isnt false
  if fsPersist
    try
      stat = fs.statSync storageDir
      throw new Error 'memoBuilder must receive a directory' unless stat.isDirectory()
    catch err # none was received
      if err.code is 'ENOENT'
        fs.mkdirSync storageDir
      else
        throw err

  # Lê um arquivo no storageDir. Retorna defaultVal caso a leitura falhe.
  readFile = (fileName, defaultVal=null)->
    try
      fs.readFileSync "#{storageDir}/#{fileName}", 'utf8'
    catch
      defaultVal

  # Lê um arquivo JSON no storageDir. O retorno padrão é um hash vazio.
  readJSON = (fileName)->
    JSON.parse readFile "#{fileName}.json", '{}'

  # Escreve um arquivo no storageDir.
  writeFile = (fileName, data, callback=(->))->
    if fsPersist
      fs.writeFile "#{storageDir}/#{fileName}", data, callback
    else
      do callback

  # Escreve um arquivo no storageDir.
  writeJSON = (fileName, data, callback)->
    data = JSON.stringify data, null, '  '
    writeFile "#{fileName}.json", data, callback

  getStorageDir = -> storageDir

  {
    readFile: readFile
    readJSON: readJSON
    writeFile: writeFile
    writeJSON: writeJSON
    getStorageDir: getStorageDir
  }
